//Чому для роботи з input не рекомендується використовувати клавіатуру?
//тому що клавіатур є безліч, а тому складно підлаштуватися під кожну. також користувач може використовувати інший пристрій, на якому немає клавіатури

window.addEventListener('keydown', (event) => {

    const activeBtn = document.querySelector('.active')
    const btns = document.querySelectorAll('.btn')

    btns.forEach(elem => {
        if (event.key === elem.textContent.toLowerCase()||event.key === elem.textContent) {
            if (activeBtn && activeBtn !== elem) {
                activeBtn.classList.remove('active')
            }
            elem.classList.toggle('active')
        } 
    })
})